defmodule ExRabbitMQRiakBridge.Mixfile do
  use Mix.Project

  def project do
    [app: :exrabbitmqriakbridge,
     version: "0.5.0",
     elixir: "~> 1.5",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [extra_applications: [:logger],
     mod: {ExRabbitMQRiakBridge.Application, []}]
  end

  defp deps do
    [
      {:credo, "~> 0.8.1", runtime: false},
      {:riak, "~> 1.1"},
      {:amqp, "~> 0.2.2"},
      {:poison, "~> 3.1"},
      {:gen_stage, "~> 0.12.1"},
      {:dialyxir, "~> 0.5.0", only: [:dev]},
      {:exrabbitmq, git: "https://github.com/StoiximanServices/exrabbitmq.git", tag: "v2.5.1"},
      {:distillery, "~> 1.5"},
    ]
  end
end
