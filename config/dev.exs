use Mix.Config

config :pooler, pools:
  [
    [
      name: :riak,
      group: :riak,
      max_count: 15,
      init_count: 10,
      start_mfa: {Riak.Connection, :start_link, ['192.168.42.111', 100]}
    ]
  ]

config :logger,
  backends: [:console],
  level: :debug

config :logger, :console,
  colors: [enabled: true],
  format: "$time $metadata[$level] $message\n",
  metadata: [:module, :function, :line]

config :exrabbitmq, :riak_kv_connection,
  username: "riak_kv",
  password: "riak_kv",
  host: "localhost",
  vhost: "riak_kv"

config :exrabbitmq, :riak_kv_queue,
  queue: "riak_kv",
  queue_opts: [durable: true],
  consume_opts: [no_ack: false]
