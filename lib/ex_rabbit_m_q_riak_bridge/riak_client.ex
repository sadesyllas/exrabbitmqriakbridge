defmodule ExRabbitMQRiakBridge.RiakClient do
  @moduledoc false
  @module __MODULE__

  require Logger

  alias ExRabbitMQRiakBridge.RabbitMQ.Consumer
  alias ExRabbitMQRiakBridge.RabbitMQ.Producer

  def start_link(event) do
    Task.start_link(@module, :process_event, [event])
  end

  def process_event({payload_json, %{delivery_tag: delivery_tag, reply_to: reply_to, correlation_id: correlation_id}}) do
    with {:ok, payload} <- Poison.decode(payload_json) do
      riak_do(payload["action"], payload["bucket"], payload["key"], payload["value"], reply_to, correlation_id)

      Consumer.ack(delivery_tag)
    else
      _ ->
        Consumer.reject(delivery_tag, requeue: false)

        Logger.error("received invalid json payload or invalid parameters: "
          <> "[payload]=#{payload_json}, "
          <> "[reply_to]=#{reply_to}, "
          <> "[correlation_id]=#{correlation_id}")
    end
  end

  def bucket_keys(bucket) do
    bucket
    |> Riak.Bucket.keys()
    |> elem(1)
  end

  def clear_bucket(bucket) do
    bucket
    |> Riak.Bucket.keys()
    |> elem(1)
    |> Enum.each(fn key -> Riak.delete(bucket, key) end)
  end

  defp riak_do(nil, _bucket, _key, _value, _reply_to, _correlation_id) do
    Logger.error("received nil action in request")
  end

  defp riak_do("update", bucket, key, value, reply_to, correlation_id) do
    r_obj = Riak.Object.create(bucket: bucket, key: key, data: value)

    riak_do_after_update(r_obj, reply_to, correlation_id, Riak.put(r_obj))
  end

  defp riak_do("get", bucket, key, _, reply_to, correlation_id) do
    reply =
      case Riak.find(bucket, key) do
        nil -> %{bucket: bucket, key: key, error: "NOT_FOUND"}
        r_obj -> %{bucket: bucket, key: key, value: r_obj.data}
      end
    Producer.send(reply_to, Poison.encode!(reply), correlation_id)
  end

  defp riak_do_after_update(r_obj, reply_to, correlation_id, nil) do
    Logger.error("could not update object in riak with bucket \"#{r_obj.bucket}\", key \"#{r_obj.key}\" and data \"#{r_obj.data}\"")

    if reply_to !== :undefined do
      Producer.send(
        reply_to,
        Poison.encode!(%{bucket: r_obj.bucket, key: r_obj.key, error: "UPDATE_ERROR"}), correlation_id)
    end
  end

  defp riak_do_after_update(r_obj, reply_to, correlation_id, _) do
    Logger.info("updated object in riak with bucket \"#{r_obj.bucket}\", key \"#{r_obj.key}\" and data \"#{r_obj.data}\"")

    if reply_to !== :undefined do
      Producer.send(reply_to, Poison.encode!(%{bucket: r_obj.bucket, key: r_obj.key, data: r_obj.data}), correlation_id)
    end
  end
end
