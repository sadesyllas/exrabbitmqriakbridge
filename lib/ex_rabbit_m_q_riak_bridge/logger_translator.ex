defmodule ExRabbitMQRiakBridge.LoggerTranslator do
  @moduledoc false

  def translate(_min_level, :error, :format,
    {_msg, [_pid, {:tcp_closed, _port}, _state, :disconnected]}) do
    :skip
  end

  def translate(_min_level, :error, :format,
    {_msg, [_pid, {:DOWN, _ref, :process, _down_pid, :basic_cancel}, _state, _data]}) do
    :skip
  end

  def translate(_min_level, :info, :format, {_msg, [_pid, _other_pid, :basic_cancel]}) do
    :skip
  end

  def translate(_min_level, _level, _kind, _message) do
    :none
  end
end
