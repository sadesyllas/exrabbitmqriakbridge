defmodule ExRabbitMQRiakBridge.RabbitMQ.Producer do
  @moduledoc false
  @module __MODULE__

  use GenServer
  use ExRabbitMQ.Producer

  require Logger

  def start_link() do
    GenServer.start_link(@module, nil, name: @module)
  end

  def init(nil) do
    config = Application.get_env(:exrabbitmqriakbridge, :riak_kv) || []
    connection_config_key = Keyword.get(config, :connection_config_key, :riak_kv_connection)

    xrmq_init(connection_config_key, nil)

    {:ok, nil}
  end

  def send(to, message, correlation_id) do
    Logger.info("sending to queue: #{to} with message: #{inspect(message)} and correlation_id: #{correlation_id}")

    GenServer.cast(@module, {:send, to, message, correlation_id})
  end

  def handle_cast({:send, to, message, correlation_id} = msg, state) do
    Logger.info("publishing to queue: #{to} with message: #{inspect(message)} and correlation_id: #{correlation_id}")

    {exchange, routing_key} =
      case String.split(to, "/", trim: true) do
        [routing_key] -> {"", routing_key}
        [exchange, routing_key] -> {exchange, routing_key}
      end

     ok? = xrmq_basic_publish(message, exchange, routing_key, persistent: true, correlation_id: correlation_id)

     if ok? !== :ok do
      GenServer.cast(@module, msg)
     end

    {:noreply, state}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end
end
