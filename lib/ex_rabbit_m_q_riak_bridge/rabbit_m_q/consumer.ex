defmodule ExRabbitMQRiakBridge.RabbitMQ.Consumer do
  @moduledoc false
  @module __MODULE__

  use GenStage
  use ExRabbitMQ.Consumer, GenStage

  require Logger

  alias __MODULE__

  defstruct messages: []

  def start_link() do
    GenStage.start_link(@module, :ok, name: @module)
  end

  def ack(delivery_tag) do
    GenStage.cast(@module, {:ack, delivery_tag})
  end

  def reject(delivery_tag, opts \\ []) do
    GenStage.cast(@module, {:reject, delivery_tag, opts})
  end

  def init(:ok) do
    config = Application.get_env(:exrabbitmqriakbridge, :riak_kv) || []
    connection_config_key = Keyword.get(config, :connection_config_key, :riak_kv_connection)
    queue_config_key = Keyword.get(config, :queue_config_key, :riak_kv_queue)

    new_state =
      xrmq_init(connection_config_key, queue_config_key, %Consumer{})
      |> xrmq_extract_state()

    {:producer, new_state}
  end

  def handle_cast({:ack, delivery_tag} = msg, state)  do
    case xrmq_basic_ack(delivery_tag, state) do
      {:error, _, _} -> GenServer.cast(@module, msg)
      _ -> nil
    end

    {:noreply, [], state}
  end

  def handle_cast({:reject, delivery_tag, opts} = msg, state) do
    case xrmq_basic_reject(delivery_tag, opts, state) do
      {:error, _, _} -> GenServer.cast(@module, msg)
      _ -> nil
    end

    {:noreply, [], state}
  end

  def xrmq_basic_deliver(payload, meta, %{messages: messages} = state) do
    new_state = %{state | messages: messages ++ [{payload, meta}]}

    {:noreply, [{payload, meta}], new_state}
  end

  def handle_info(_, state) do
    {:noreply, [], state}
  end

  def handle_demand(demand, %{messages: messages} = state) do
    {to_send, to_keep} = Enum.split(messages, demand)

    state = %{state | messages: to_keep}

    {:noreply, to_send, state}
  end
end
