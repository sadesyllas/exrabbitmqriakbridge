defmodule ExRabbitMQRiakBridge.Application do
  @moduledoc false

  use Application

  require Logger

  def start(_type, _args) do
    Logger.add_translator({ExRabbitMQRiakBridge.LoggerTranslator, :translate})

    children = [
      child_spec(ExRabbitMQ.ConnectionSupervisor),
      child_spec(ExRabbitMQRiakBridge.RabbitMQ.Consumer),
      child_spec(ExRabbitMQRiakBridge.RabbitMQ.Producer),
      child_spec(ExRabbitMQRiakBridge.RiakClient.Supervisor),
    ]

    opts = [
      strategy: :one_for_one,
      name: ExRabbitMQRiakBridge.Supervisor,
      max_restarts: 1000,
      max_seconds: 1,
    ]

    Supervisor.start_link(children, opts)
  end

  defp child_spec(module) do
    Supervisor.child_spec(module, start: {module, :start_link, []}, restart: :permanent)
  end
end
