defmodule ExRabbitMQRiakBridge.RiakClient.Supervisor do
  @moduledoc false
  @module __MODULE__

  alias ExRabbitMQRiakBridge.RabbitMQ.Consumer

  def child_spec(_) do
    %{
      id: @module,
      type: :supervisor,
    }
  end

  def start_link() do
    import Supervisor.Spec

    children = [
      worker(ExRabbitMQRiakBridge.RiakClient, [], restart: :temporary)
    ]

    riak_pooler_pool =
      Application.get_env(:pooler, :pools)
      |> Enum.filter(fn p -> p[:name] === :riak end)
      |> List.first()
    min_demand = riak_pooler_pool[:init_count]
    max_demand = riak_pooler_pool[:max_count]

    opts = [
      strategy: :one_for_one,
      subscribe_to: [{Consumer, [min_demand: min_demand, max_demand: max_demand]}],
    ]

    ConsumerSupervisor.start_link(children, opts)
  end
end
